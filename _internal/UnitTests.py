# Basic unit testing framework

class UnitTests:
    def __init__ (self, title):
        self.tests = []
        self.names = []
        self.title = title

# Add a test, takes a name and a function that returns true or false
    def addtest (self, name, func):
        self.names.append(name)
        self.tests.append(func)

# Run all tests
    def test (self):
        print ("--------------------\nTesting: " + self.title + "\n--------------------")
        for i,n in zip(self.tests,self.names):
            if callable(i):
                try:
                    self.result = i()
                except:
                    self.result = False
                if len(n) > 21:
                    n = n[:18] + "..."
                if self.result:
                    print (n + ':\tPass')
                else:
                    print (n + ':\tFail')
            else:
                print (n + ':\tError: i not callable')

    def assertequals (a, b):
        return a == b

# Run all tests
    def __call__ (self):
        self.test()

# Test Functor Class
from Functor import Functor
F = Functor
FunctorTests = UnitTests("Functor")
FunctorTests.addtest("Scalar Equality",lambda: UnitTests.assertequals(F(1).value,1))
FunctorTests.addtest("Scalar Inequality",lambda: not UnitTests.assertequals(F(1).value, 2))
FunctorTests.addtest("Functor Operator",lambda: UnitTests.assertequals((F(lambda x: x + 1)(4)).value,5))
FunctorTests.addtest("Functor Composition",lambda: UnitTests.assertequals((F(lambda x: x + 1)(F(lambda x: x * 2))(2)).value,5))
FunctorTests.addtest("Multiple Arguments",lambda: UnitTests.assertequals(((F(lambda a, b: a + b) + F(lambda a, b: a + b))(3,4)).value,14))
FunctorTests.addtest("Scalar Multiplication of Multiple Arguments",lambda: UnitTests.assertequals(((2 * F(lambda a, b: a + b))(3,4)).value, 14))
add = lambda a, b:  a + b
ao = lambda a: a + 1
at = lambda a: ao(ao(a))
FunctorTests.addtest("Functions as Multiple Arguments Composition",lambda: UnitTests.assertequals((F(add)(F(ao), F(at))(2)).value,7))
FunctorTests.addtest("Compose Functions as Functors",lambda: UnitTests.assertequals((F(F(ao))(F(F(at)))(-1)).value,2))
FunctorTests.addtest("Compose Functions with Multiple Arguments",lambda: UnitTests.assertequals(((F(add)(F(add),F(add)))(2,3)).value,10))

FunctorTests.test()



# Test the Vector class

from Vector import Vector
V = Vector
VectorTests = UnitTests("Vector")
VectorTests.addtest("Multiple Functions",lambda: UnitTests.assertequals((V(ao,at)(1,2)).getrawvalue(),(2,4))) # Fails do to __call__ being unfinished
VectorTests.addtest("Scalar Multiplication",lambda: UnitTests.assertequals((2 * V(1,2)).getrawvalue(),(2,4)))
VectorTests.addtest("Scalar Multiplication as Vector",lambda: UnitTests.assertequals((V(2) * V(1,2)).getrawvalue(),(2,4)))
#VectorTests.addtest(,UnitTests.assertequals())
#VectorTests.addtest(,UnitTests.assertequals())
#VectorTests.addtest(,UnitTests.assertequals())
#VectorTests.addtest(,UnitTests.assertequals())
#VectorTests.addtest(,UnitTests.assertequals())
#VectorTests.addtest(,UnitTests.assertequals())
#VectorTests.addtest(,UnitTests.assertequals())

VectorTests.test()
