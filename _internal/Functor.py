class Functor:

# The Functor class handles the elements of Vectors
# It simplifies the process of Vector operations by handling things like int + function

    __slots__ = ['value']

# Simply take store the value given or None.
    def __init__ (self, value = None):
        self.value = value

# If the value passed is a Functor, use the value of that Functor for self.value
        if type(self.value) is Functor:
            self.value = self.value.value


# Call will either return the value directly, or call the function stored if possible
    def __call__ (self, *args):
        if len(args) == 1:
            if type(args[0]) is Functor:
                temp = args[0].value
                del (args)
                args = (temp,)
            if callable (self.value):
                if callable(args[0]):
                    return Functor(lambda *x: self.__call__(args[0](*x)))
                else:
                    return Functor(self.value(args[0]))
        if callable (self.value):
            return Functor(self.value(*args))
        return Functor(self.value)

# Get simply calls __call__
    def get (self, arg):
        return self.__call__(arg)

# Simply print the contents
    def __repr__ (self):
        return str(self.value)

# Addition of Functors is defined here
    def __add__ (self, other):
        if callable(self.value):
            if callable(other.value):
                return Functor(lambda *args: self.value(*args) + other.value(*args))
            else:
                return Functor(lambda *args: self.value(*args) + other.value)
        else:
            if callable(other.value):
                return Functor(lambda *args: self.value + other.value(*args))
            else:
                return Functor(self.value + other.value)

# __radd__ is passed back to __add__ except in the case of 0 + Functor.
# This exception is used to ensure that sum([list of Functors]) works properly.
    def __radd__ (self, other):
        if other == 0:
            return self
        return self.__add__(other)

# Multiplication is handled here
    def __mul__ (self, other):
        if type(other) is int or type(other) is float:
            other = F(other)
        if callable(self.value):
            if callable(other.value):
                return Functor(lambda *args: self.value(*args) * other.value(*args))
            else:
                return Functor(lambda *args: self.value(*args) * other.value)
        else:
            if callable(other.value):
                return Functor(lambda *args: self.value * other.value(*args))
            else:
                return Functor(self.value * other.value)

# Send __rmul__ to __mul__
    def __rmul__ (self, other):
        return self.__mul__(other)

# Subtraction is handled here
    def __sub__ (self, other):
            if callable(self.value):
                if callable(other.value):
                    return Functor(lambda *args: self.value(*args) - other.value(*args))
                else:
                    return Functor(lambda *args: self.value(*args) - other.value)
            else:
                if callable(other.value):
                    return Functor(lambda *args: self.value - other.value(*args))
                else:
                    return Functor(self.value - other.value)

# Division is handled here
    def __truediv__ (self, other):
        if callable(self.value):
            if callable(other.value):
                return Functor(lambda *args: self.value(*args) / other.value(*args))
            else:
                return Functor(lambda *args: self.value(*args) / other.value)
        else:
            if callable(other.value):
                return Functor(lambda *args: self.value / other.value(*args))
            else:
                return Functor(self.value / other.value)

# Pass __rdiv__ back to other.__div__(self) with other being cast as a Functor.
# This ensures that the operation will be completed without error
    def __rtruediv__ (self, other):
        return Functor(other).__div__(self)

# Purely cosmetic
F = Functor
