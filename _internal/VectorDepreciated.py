import math

class Vector:

# Each instance of the Vector class contains a single list.
# This list acts as a mathematical vector.

    __slots__ = ['dimensionality', 'value', 'isoperator']

    def __init__ (self, *args):
        self.value = args
        self.isoperator = False

        if len(self.value) == 1:
            self.value = args[0]

        # Ensure that the value is stored as a tuple and handle the isoperator flag
        if type(self.value) is int or type(self.value) is float or callable(self.value):
            self.value = (self.value,)
        elif type(self.value) is list:
            self.value = tuple(self.value)
        for i in self.value:
            if callable(i):
                self.isoperator = True

        self.dimensionality = len(self.value)

# Returns the magnitude as calculated from the Pythagorean Theorm
    def getmagnitude (self):
        return math.sqrt(sum([i**2 for i in self.value]))

# Return the value as tuple for dimensionality > 1 or singleton for dimensionality = 1
    def getvalue (self, *args):
        if self.dimensionality > 1:
            if len(args) == 1:
                return self.value[args[0]]
            elif len(args) > 1:
                return tuple([self.value[i] for i in args])
            return self.value
        if self.dimensionality == 1:
            return self.value[0]

# Decorator function that adds dimentional consistancy checking
    def checkdimension (func):
        def decorator (self, other):
            if self.dimensionality != other.dimensionality:
                raise ArithmeticError ('Dimensional inconsistancy')
            return func(self, other)
        return decorator

# Returns the angle between two vectors in radians
    @checkdimension
    def getanglebetween (self, other):
        return math.acos((self * other) / (self.getmagnitude() * other.getmagnitude()))

# Overload + operator to work as expected for Vectors
    @checkdimension
    def __add__ (self, other):
        if not self.isoperator and not other.isoperator:
            return Vector([s + o for s,o in zip(self.value, other.value)])
        elif self.isoperator and not other.isoperator:
            return Vector([lambda *args: s(*args) + o for s,o in zip(self.value, other.value)])
        elif not self.isoperator and other.isoperator:
            return Vector([lambda *args: o(*args) + s for s,o in zip(self.value, other.value)])
        elif self.isoperator and other.isoperator:
            return Vector([lambda *args: o(*args) + s(*args) for s,o in zip(self.value, other.value)])

# Send __radd__ to __add__
    def __radd__ (self, other):
        if other == 0:
            return self
        return self.__add__(other)

# Overload - operator to work as expected for Vectors
    @checkdimension
    def __sub__ (self, other):
        if not self.isoperator and not other.isoperator:
            return Vector([s - o for s,o in zip(self.value, other.value)])
        elif self.isoperator and not other.isoperator:
            return Vector([lambda *args: s(*args) - o for s,o in zip(self.value, other.value)])
        elif not self.isoperator and other.isoperator:
            return Vector([lambda *args: s - o(*args) for s,o in zip(self.value, other.value)])
        elif self.isoperator and other.isoperator:
            return Vector([lambda *args: s(*args) - o(*args) for s,o in zip(self.value, other.value)])

# Overload * operator to behave as a dot product or scalar multiplication
    def __mul__ (self, other):

# Handle scalar multiplication

    # Handle the case of other not being a Vector
        if type(other) is int or type(other) is float: # If other is a scalar
            if not self.isoperator:
                return Vector([i * other for i in self.value])
            else:
                return Vector([lambda *args, i=i: i(*args) * other for i in self.value])####### TODO: FIX!
        elif callable(other):
            return Vector([lambda *args, i=i: i * other(*args) for i in self.value])

    # And if it is a Vector?
        elif type(other) is Vector:
            if len(other.value) == 1:
                return self * other.getvalue()
            elif len(self.value) == 1:
                return self.getvalue() * other


# Send the rest to multiply
        return self._multiply(other)

# Send __rmul__ to __mul__
    def __rmul__ (self, other):
        return self.__mul__(other)

# Standard multiplication function for Vectors with other Vectors
    @checkdimension
    def _multiply(self, other):
        if not self.isoperator and not other.isoperator:
            return Vector(sum([s * o for s,o in zip(self.value, other.value)]))
        elif self.isoperator and not other.isoperator:
            return sum([Vector(lambda *args: o * s(*args)) for s,o in zip(self.value, other.value)])
        elif not self.isoperator and other.isoperator:
            return sum([Vector(lambda *args: o(*args) * s) for s,o in zip(self.value, other.value)])
        elif self.isoperator and other.isoperator:
            return sum([Vector(lambda *args: o(*args) * s(*args)) for s,o in zip(self.value, other.value)])

# Operates a vector function on arguments
    def operate(self, *args):
        if self.isoperator:
            return Vector([i(*args) for i in self.value])

# Copies without creating link.
    def copy(self):
        return Vector(self.value)


# Purely cosmetic
V = Vector
Vector.show = Vector.getvalue
