from Functor import Functor

class Vector:

# Initilizer values for Vector must be either individual values or a single tuple
    def __init__ (self, *args):
        self.value = args

# Ensure that value isn't a list
        if type(self.value[0]) is list:
            self.value = tuple(self.value[0])

# Ensure that self.value is a single, flat tuple
        if len(self.value) > 0:
            if type(self.value[0]) is tuple:
                self.value = sum(args,())

# Convert the elements to functors
        for i in self.value:
            self.value = tuple(Functor(i) for i in self.value)

# Return the value as a tuple
    def getrawvalue (self, *args):
        return tuple([i(*args).value for i in self.value])

# Return the value of the vector as operator is applied
    def __call__ (self, *args):
        return Vector([i(*args) for i in self.value])

# Overload the + operator
    def __add__ (self, other):
        return Vector(tuple([s + o for s,o in zip(self.value,other.value)]))

# Send __aradd__ to __add__
    def __radd__ (self, other):
        if other == 0:
            return self
        return self.__add__(other)

# Overload the * operator
    def __mul__ (self, other):
        if type(other) is int or type(other) is float or (type(other) is Vector and len(other.value) == 1) or type(other) is Functor:
            return self._scalarmul(other)
        return sum([s * o for s,o in zip(self.value,other.value)])

    def __rmul__ (self, other):
        return self.__mul__(other)

    def _scalarmul (self, other):
        if type (other) is Vector:
            other = other.value
        return Vector([i * other for i in self.value])
