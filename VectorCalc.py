# Lanugage: Python 3.4
# Author:   Thomas Marshall

import math
from bin.Vector import Vector

class Field:

# Each instance of Field is a tensor field.
    def __init__ (self, dimensionality, nodes = []):
        self.dimensionality = dimensionality
        self.nodelist = NodeList(nodes)

# Adds a point to the field
    def addnode (self, position, value = None):
        self.nodelist.addnode(position, value)


class NodeList:

# NodeList will handle the indexing and addressing of instances of Node
# This function will allow for Nodes to be indexed by both position and an identifier
#
# This function acts as a buffer between th Field class and the Node class.
# It allows the Field class to dynamically interact with a list of Nodes.
# It also handles identifiers, allowing instances of Node to be handled by both position and identifier.
#
# TODO: Imporove this. Standardize the way vectors interact with touples.

    __slots__ = ['nodes', 'posdic']

    def __init__ (self, nodes = []):
        self.nodes = nodes

# Set up the node dictionary and initilize it with data from sef.nodes
        self.posdic = {}
        for i in self.nodes:
            self.posdic[i.position.value] = i.identifier

# Adds a node to the list
    def addnode (self, position, value = None):
        if type(position) is Vector:
            position = position.value
        if position not in nodedic.keys():
            self.posdic[position] = len(self.nodes)
            self.nodes.append(Node(position, len(self.nodes), value))
        return

# Decorator that handles identifying by idnetifier or position
    def handleidentifier (func):
        def decorator(self, incoming, *args):
            if type(incoming) is tuple:
                incoming = self.nodes[self.posdic[incoming]].identifier
            return func(self, incoming, *args)
        return decorator

# Returns a given node from its identifier
    @handleidentifier
    def getnode (self, identifier):
        return self.nodes[identifier]

# Returns the position of a node
    @handleidentifier
    def getpos (self, identifier):
        return self.nodes[identifier].position

# Changes a node's position
    @handleidentifier
    def changeposition (self, identifier, value):
        pass
        #TODO: Add this code




class Node:

# Each instance of Node contains a vector and a value.
# Instances of Node are intended to be used as the elements of an array in an instance of Field.
# Node also deals with basic mathematical capabilities relating points in mathematical fields.

    __slots__ = ['dimensionality', 'value', 'position', 'identifier']

    def __init__ (self, position, identifier, value = None):
        self.identifier = identifier
        if type(position) is tuple:
            self.position = Vector(position)
        elif type(position) is Vector:
            self.position = position.copy()
        elif type(position) is list:
            self.position = Vector(tuple(position))
        self.value = value
        self.dimensionality = self.position.dimensionality

# Return the vector formed by traveling form self's position to other's
    def getvectorto (self, other):
        return other.position - self.position

# Return the distance between this node and other
    def getdistanceto (self, other):
        return (getvectorto(other)).getmagnitude()

# Return the value of the angle othera, self, otherb in radians
    def getanglebetween (self, othera, otherb):
        return self.getvectorto(othera).getanglebetween(self.getvectorto(otherb))
